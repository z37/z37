---
layout: post
title:  "Post Template for jekyll blog site"
date:   2020-11-05 00:38:10 -0600
categories: jekyll Post
permalink: /:categories  #tells to put the url of the categories.
# can also be /:categories/:day/:year/:month/:title.html
author: "JV"
---


<!--this block make a for loop for the hyperlinks of the posts in the static site. Also added the if statment that highlights in color orange the link of the post you are on-->
### Post Navigation Pane
{% for post in site.posts %}
<li> <a style="{% if page.url == post.url %} color:#CE534D;{% endif%}" href="{{ post.url }}"> {{post.title}}</a> </li>
{% endfor %} 
---

You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways, but the most common way is to run `jekyll serve`, which launches a web server and auto-regenerates your site when a file is updated.

Jekyll requires blog post files to be named according to the following format:

`YEAR-MONTH-DAY-title.MARKUP`

Where `YEAR` is a four-digit number, `MONTH` and `DAY` are both two-digit numbers, and `MARKUP` is the file extension representing the format used in the file. After that, include the necessary front matter. Take a look at the source for this post to get an idea about how it works.

Jekyll also offers powerful support for code snippets:
Example ruby code:
{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

Here is a code highlight for a json file.

{% highlight javascript %}
{
  "firstName": "Mickey",
  "lastName": "Mouse",
  "age": 9,
  "married": false,
}
{% endhighlight %}

Here is a code highlighy syntax for c++ code.


#include <iostream>

{% highlight c++ %}
// Your First C++ Program
int main() {
    std::cout << "Hello World!";
    return 0;
}
{% endhighlight %}


### HTML & CSS

* HTML.- hypertext markup language.
* Represent the technology that boosts the web.
* HTML5 -> Core structure -> components and infrastructure.
* CSS3 -> Colors, styles.
* JavaScript JS -> Behavior, for dynamic content.
* Annotation contents.
* Definition of document structure.
* 3 Core languages for the web technologies.



[jekyll-docs]: https://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
